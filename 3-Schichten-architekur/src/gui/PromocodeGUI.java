package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.IPromoCodeLogic;

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;
	private JPanel contentPane;
	private JLabel lblPromocode;

	/**
	 * Create the frame.
	 */
	public PromocodeGUI(IPromoCodeLogic logic) {
		setTitle("PromotionCodeGenerator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 350);
		setBackgroundImage(contentPane);
		contentPane = (JPanel)getContentPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		contentPane.add(new JLabel(""));
		contentPane.add(new JLabel(""));
		contentPane.add(new JLabel(""));
		contentPane.add(new JLabel(""));
		contentPane.add(new JLabel(""));
		contentPane.add(new JLabel(""));
		
		JLabel lblBeschriftungPromocode = new JLabel("Promotioncode f�r 2-fach IT$ in einer Stunde");
		lblBeschriftungPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblBeschriftungPromocode.setBackground(Color.WHITE);
		lblBeschriftungPromocode.setOpaque(true);
		contentPane.add(lblBeschriftungPromocode);
		this.setVisible(true);
		
		JButton btnNewPromoCode = new JButton("Generiere neuen Promotioncode");
		btnNewPromoCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String code = logic.getNewPromoCode();
				lblPromocode.setOpaque(true);
				lblPromocode.setText(code);
			}
		});
		contentPane.add(btnNewPromoCode);
		
		lblPromocode = new JLabel("");
		lblPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromocode.setBackground(Color.WHITE);
		
		lblPromocode.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		contentPane.add(lblPromocode);
		
		setResizable(false);
		
		
	}
	
	public void setBackgroundImage(JPanel contentPane) {			//https://ichef.bbci.co.uk/images/ic/960x540/p053nddk.jpg
	
		
		try {
		    final Image backgroundImage = ImageIO.read(getClass().getResource("wallpaper.jpg"));
		    Image newimg = backgroundImage.getScaledInstance( 450, 350,  java.awt.Image.SCALE_SMOOTH ) ; 
		    setContentPane(new JPanel(new GridLayout(10,1)) {
		        @Override public void paintComponent(Graphics g) {
		            g.drawImage(newimg, 0, 0, null);
		        }
		    });
		   
		} catch (IOException e) {
		}
	}

}
